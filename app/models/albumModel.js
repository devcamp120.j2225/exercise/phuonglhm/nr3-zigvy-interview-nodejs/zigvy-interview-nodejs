const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const albumSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    userId: {
        type: mongoose.Types.ObjectId,
        required: true, 
        ref: "user"
    },
    title: {
        type: String,
        required: true
    }

})

module.exports = mongoose.model('album', albumSchema)