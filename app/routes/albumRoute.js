const express = require('express')
const { createAlbum, getAllAlbum, getAlbumById, updateAlbumById, deleteAlbumById, getAlbumOfUser } = require('../controllers/albumController')
const albumRouter = express.Router()

albumRouter.post('/albums', createAlbum)

albumRouter.get('/albums', getAllAlbum)

albumRouter.get('/albums/:albumId', getAlbumById)

albumRouter.put('/albums/:albumId', updateAlbumById)

albumRouter.delete('/albums/:albumId', deleteAlbumById)

albumRouter.get('/users/:userId/albums', getAlbumOfUser)

module.exports = {albumRouter}