const express = require('express')
const { getAllComment, updateCommentById, getAllCommentById, deleteCommentById, getCommentOfPost, createComment } = require('../controllers/commentController')

const commentRouter = express.Router()

commentRouter.post('/comments', createComment)

commentRouter.get('/comments', getAllComment)

commentRouter.get('/comments/:commentId', getAllCommentById)

commentRouter.get('/posts/:postId/comments', getCommentOfPost)

commentRouter.put('/comments/:commentId', updateCommentById)

commentRouter.delete('/comments/:commentId', deleteCommentById)

module.exports = {commentRouter}