const express = require('express')
const { createPost, getAllPosts, updatePostById, getPostById, deletePostById, getPostOfUser } = require('../controllers/postController')

const postRouter = express.Router()

postRouter.post('/posts', createPost)

postRouter.get('/posts',getAllPosts)

postRouter.get('/posts/:postId', getPostById)

postRouter.get('/users/:userId/posts', getPostOfUser)

postRouter.put('/posts/:postId',updatePostById)

postRouter.delete('/posts/:postId', deletePostById)

module.exports = {postRouter}