const postModel = require('../models/postModel')
const mongoose = require('mongoose')

const createPost = (request, response) => {
    //B1: thu thập dữ liệu
    let bodyRequest = resquest.body;
    //b2: validate dữ liệu
    if(!mongoose.Types.ObjectId(userId)) {
        return response.status(400).json({
            message: "user is required"
        })
    }
    if(!title) {
        return response.status(400).json({
            message: "title is required"
        })
    }
    if(!body) {
        return response.status(400).json({
            message: "body is required"
        })
    }

    //b3: xử lý dữ liệu và trả ra kết quả
    let createNewPost = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyRequest.userId,
        title: bodyRequest.title,
        body: bodyRequest.body
    }

    postModel.create(createNewPost,(error, data) => {
        if (error) {
            response.status(500).json({
                message: `Internal server error: ${error.message}`
            })
        } else {
            response.status(201).json({
                data
            })
        }
    });
}

//get all Posts
const getAllPosts = (request, response) => {
    //B1. thu thập dữ liệu 
    let userId = request.query.userId //GET “/posts?userId=:userId
    //B2. kiểm tra dữ liệu (bỏ qua)
    //B3. thực hiện thao tác dữ liệu
    let condition = {};
    if (userId){
        condition.userId = userId;
    }
    postModel.find(condition,(error, data) => {
        if (error) {
            response.status(500).json({
                message: `Internal server error: ${error.message}`
            })
        } else {
            response.status(200).json({
                data
            })
        }
    })
};
//get a post by id
const getPostById = (request, response) => {
    //B1. thu thập dữ liệu (bỏ qua)
    let id = request.params.postid;

    //B2. kiểm tra dữ liệu 
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "postId is invalid!"
        })
    } 
        //B3. thực hiện thao tác dữ liệu
        postModel.findById(id, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        })
    }
//update a post by id
const updatePostById = (request, response) => {
    //B1. Thu thập dữ liệu
    let postId = request.params.postId;
    let bodyRequest = request.body;

    //B2. Kiểm tra dữ liêu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        response.status(400).json({
            message: "postId is invalid!"
        })
    } 
    if(!mongoose.Types.ObjectId(userId)) {
        return response.status(400).json({
            message: "userId is invalid"
        })
    } 
    if (!bodyRequest.title || !bodyRequest.body) {
        response.status(400).json({
            message: " Title and post body are required!"
        })
    } 
        //B3: thực hiện các thao tác nghiệp vụ
        let updatePost = {
            userId: bodyRequest.userId,
            title: bodyRequest.title,
            body: bodyRequest.body,
        }
        postModel.findByIdAndUpdate(postId, updatePost, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        });
    }

//delete a post by id
const deletePostById = (request, response) => {
    //B1. Thu thập dữ liệu
    let postId = request.params.postId;

    //B2. Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        response.status(400).json({
            message: "postId is invalid!"
        })
    } 
        //B3: thực hiện các thao tác nghiệp vụ
        postModel.findByIdAndDelete(postId, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(204).json({
                    message: `successfully deleted post ${postId}`,
                })
            }
        })
    }
const getPostOfUser = (request, response) => {
    //B1. thu thập dữ liệu (bỏ qua)
    let userId = request.params.userid;
    console.log(userId);
    //B2. kiểm tra dữ liệu 
    let condition = {};
    if (userId) {
        condition.userId = userId;
    }
    //B3. thực hiện thao tác dữ liệu
        postModel.find(condition, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        })
};

module.exports = {createPost, getAllPosts,getPostById,getPostOfUser,updatePostById,deletePostById }