const mongoose = require('mongoose')
const commentModel = require('../models/commentModel')

const createComment = (request,response) => {
    //B1: thu thập dữ liệu
    let bodyReq = request.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(bodyReq.postId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "postId is invalid"
        })
    }
    if(!bodyReq.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if(!bodyReq.email) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        })
    }
    if(!bodyReq.body) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "body is required"
        })
    }
    
    //B3: xử lý dữ liệu và trả về kết quả
    const newComment = {
        _id: mongoose.Types.ObjectId(),
        postId: bodyReq.postId,
        name: bodyReq.name,
        email: bodyReq.email,
        body: bodyReq.body,
    }

    commentModel.create(newComment, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Create comment successfully !!!",
                data: data
            })
        }
    }) 
}

const getAllComment = (req, res) => {
    //B1: thu thập dữ liệu
    let postId = req.query.postId; //GET “/comments?postId=:postId”
    //B2: validate dữ liệu
    let condition = {};

    if(postId){
        condition.postId = postId;
    }
    //B3: xử lý dữ liệu và trả về kết quả
    commentModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Comment successfully !!!",
                data: data
            })
        }
    })
}

const getAllCommentById = (req, res) => {
    //B1: thu thập dữ liệu
    let commentId = req.params.commentId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "commentId is invalid"
        })
    }
    //B3: xử lý dữ liệu và trả về kết quả
    commentModel.findById(commentId, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get Comment By Id successfully !!!",
                data: data
            })
        }
    })
}

const updateCommentById = (req, res) => {
    //B1 thu thập dữ liệu
    let commentId = req.params.commentId;
    let bodyReq = req.body;
    //B2 validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "commentId in invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(bodyReq.postId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "postId is invalid"
        })
    }
    if(!bodyReq.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if(!bodyReq.email) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        })
    }
    if(!bodyReq.body) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "body is required"
        })
    }
    //B3 xử lý data và trả về kết quả
    let updateComment = {
        postId: bodyReq.postId,
        name: bodyReq.name,
        email: bodyReq.email,
        body: bodyReq.body,
    }
    commentModel.findByIdAndUpdate(commentId, updateComment,  (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Update Comment success!!!",
                data: data
            })
        }
    })
}

const deleteCommentById = (req, res) => {
    //B1: thu thập dữ liệu
    let commentId = req.params.commentId; 
    //B2 validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId)){
        res.status(400).json({
            status: "Error 400: Bad request!",
            message: "commentId is invalid"
        })
    }
    //B3 xử lý và trả về kết quả
    commentModel.findByIdAndDelete(commentId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Delete Comment successfully !"
            })
        }
    })
}


const getCommentOfPost = (req, res) => {
    //B1 thu thập dữ luệu
    let postId = req.params.postId;

    let condition = {};
    //B2 validate dữ liệu
    if(postId){
        condition.postId = postId;
    }
    //B3 xử lý và trả về kết quả
    commentModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Comment successfully !!!",
                data: data
            })
        }
    })
}

module.exports = {createComment, getAllComment, getAllCommentById, updateCommentById, deleteCommentById, getCommentOfPost}
